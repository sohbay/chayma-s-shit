#include "libft/libft.h"
#define BUFF_SIZE 50 //optional buff size
#define OBJECTS 10000 //max objects
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>




int countOccurrences(int arr[], int n, int x);
int binarySearch(int arr[], int l, int r, int x);
int count(struct Node* head, int search_for);
void push(struct Node** head_ref, int new_data);





int		main(int ac, char **av){

	char	buff[BUFF_SIZE + 1];
	int		fd;
	char	*line = NULL;
	char	**objects;
	int		i;
	int		j;
	int		ret;

	if (!(objects = malloc(sizeof(char *) * OBJECTS)))
			return (0);
	j = 0;
	i = 0;
	if (ac == 2)
	{
		fd = open(av[1], O_RDONLY);
		while ((ret = read(fd, buff, BUFF_SIZE))) //reading all the file
		{
			buff[ret] = '\0';
			if (!line)
				line = ft_strnew(1);
			line = ft_strjoin(line, buff);
		}
		if (!(objects = ft_strsplit(line, '}'))) // spliting to char** of objects
			return (0);
		while (objects[i])
		{
			j = 0;
			while ((objects[i][j] != '{' ) && objects[i][j] != '\0' /*|| (objects[i][j] != '"') && objects[i][j] != '\0'*/) //deleting every '{'
				j++;
			objects[i] = (ft_strdup(objects[i] + j + 1));

			i++;
		}
		j = 0;
		while (j < i-1) //printig results
		{	
			int k = 0;
			char* Newobjects;
			if(Newobjects == NULL)
			  exit(0);
			if (objects[j][k] = ' ')
				k++;
		    else if (objects[j][k] != ' ')
		    {
	           Newobjects[j][k++] = objects[j][k];
	        }	
		        printf("%c\n",Newobjects[k]);
		}
	
		    j++;
	}}
 /* Link list node */
struct Node 
{ 
	int data; 
	struct Node* next; 
}; 

/* Given a reference (pointer to pointer) to the head 
of a list and an int, push a new node on the front 
of the list. */
void push(struct Node** head_ref, int new_data) 
{ 
	/* allocate node */
	struct Node* new_node = 
			(struct Node*) malloc(sizeof(struct Node)); 

	/* put in the data */
	new_node->data = new_data; 

	/* link the old list off the new node */
	new_node->next = (*head_ref); 

	/* move the head to point to the new node */
	(*head_ref) = new_node; 
} 

/* Counts the no. of occurences of a node 
(search_for) in a linked list (head)*/
int count(struct Node* head, int search_for) 
{ 
	struct Node* current = head; 
	int count = 0; 
	while (current != NULL) 
	{ 
		if (current->data == search_for) 
		count++; 
		current = current->next; 
	} 
	return count; 
}
//using namespace std; 

// A recursive binary search function. It returns 
// location of x in given array arr[l..r] is present, 
// otherwise -1 
int binarySearch(int arr[], int l, int r, int x) 
{ 
	if (r < l) 
		return -1; 

	int mid = l + (r - l) / 2; 

	// If the element is present at the middle 
	// itself 
	if (arr[mid] == x) 
		return mid; 

	// If element is smaller than mid, then 
	// it can only be present in left subarray 
	if (arr[mid] > x) 
		return binarySearch(arr, l, mid - 1, x); 

	// Else the element can only be present 
	// in right subarray 
	return binarySearch(arr, mid + 1, r, x); 
} 

// Returns number of times x occurs in arr[0..n-1] 
int countOccurrences(int arr[], int n, int x) 
{ 
	int ind = binarySearch(arr, 0, n - 1, x); 

	// If element is not present 
	if (ind == -1) 
		return 0; 

	// Count elements on left side. 
	int count = 1; 
	int left = ind - 1; 
	while (left >= 0 && arr[left] == x) 
		count++, left--; 

	// Count elements on right side. 
	int right = ind + 1; 
	while (right < n && arr[right] == x) 
		count++, right++; 

	return count; 
} 