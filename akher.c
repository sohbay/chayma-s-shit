#include "libft/libft.h"
#define OBJECTS 10000 //max objects
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>



char	*rmspaces(char	*str)
{
	int		first;
	int		last;
	char	*new;
	int		i;

	last = strlen(str) - 1;
	first = 0;
	i = 0;
	while(str[first] == ' ' || str[first] == '\t' || str[first] == '\n')
		first++;
	while(str[last] == ' ' || str[last] == '\t' || str[last] == '\n')
		last--;
	new = (char *)malloc(sizeof(char *) * (last - first + 1));
	while (first <= last)
	{
		new[i] = str[first];
		first++;
		i++;
	}
	return (new);
}

// char	*clean(char *str)
// {
// 	int		occ;
// 	int		i;
// 	int		j;
// 	char	*new;

// 	i = 0;
// 	j = 0;
// 	occ = 0;
// 	while (str[i])
// 	{
// 		if (str[i] == '\n' || str[i] == '\t')
// 			occ++;
// 		i++;
// 	}
// 	new = (char *)malloc(sizeof(char *) * (strlen(str) - occ));
// 	i = 0;
// 	while (j < (strlen(str) - occ))
// 	{
// 		if(str[i] != '\n' || str[i] != '\t')
// 		{
// 			new[j] = str[i];
// 			j++;
// 		}
// 		i++
// 	;}
// 	return (new);
// }

int		main(int ac, char **av){

	char	buff[BUFF_SIZE + 1];
	int		fd;
	char	*line = NULL;
	char	**objects;
	int		i;
	int		j;
	int		ret;
	char	**zahia;

	if (!(objects = malloc(sizeof(char *) * OBJECTS)))
			return (0);
	j = 0;
	i = 0;
	if (ac == 2)
	{
		fd = open(av[1], O_RDONLY);
		while ((ret = read(fd, buff, BUFF_SIZE))) //reading all the file
		{
			buff[ret] = '\0';
			if (!line)
				line = ft_strnew(1);
			line = ft_strjoin(line, buff);
		}
		if (!(objects = ft_strsplit(line, '}'))) // spliting to char** of objects
			return (0);
		while (objects[i])
		{
			j = 0;
			while ((objects[i][j] != '{' ) && objects[i][j] != '\0' /*|| (objects[i][j] != '"') && objects[i][j] != '\0'*/) //deleting every '{'
				j++;
			objects[i] = (ft_strdup(objects[i] + j + 1));

			i++;
		}
		j = 0;
		while (j < i-1) //printig results
		{		
		  int n = strlen (*objects);
			// for (int z=0;z>n;z++) 
			// if (*objects[z]==' ') 
			// 		{ 
			// 		for(int k = z; k > (n-1); k++) 
			// 		*objects[k]=*objects[k+1]; 
			// 		} 
			printf("object [%d] :[%s]\n", j, rmspaces(objects[j]) );
			j++;
		}

	}
}
//Main for testing the function clean:
// int		main(int argc, char **argv)
// {
// 	if (argc != 2)
// 		return(0);
// 	printf("obj = |%s|\n", clean(argv[1]));
// }
